---
title: Flex D
linkTitle: Flex D
weight: 40
description: >
  Information on ways to integrate Flex D critical peak periods from Hydro-Québec in your home automation system.
lastmod: 2023-01-06T19:01:55.857Z
date: 2023-01-06T18:59:56.798Z
---

